﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherScript : MonoBehaviour
{

    public string lat, lng;
    public Material skySun, skyCloud, skyNight;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GetWeather());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator GetWeather()
    {
        WWW www = new WWW("https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lng + "&appid=8face5864fd116c8289967eafc65a9be");

        yield return www;

        Processjson(www.text);
    }

    private void Processjson(string jsonString)
    {
        WeatherData wd = JsonUtility.FromJson<WeatherData>(jsonString);

        System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
        int cur_time = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;

        if (wd.weather.Length > 0)
        {
            if (cur_time > wd.sys.sunset || cur_time < wd.sys.sunrise)
            {
                RenderSettings.skybox = skyNight;
            }
            else
            {
                switch (wd.weather[0].main)
                {
                    case "Clouds":
                    case "Atmosphere":
                    case "Snow":
                    case "Rain":
                    case "Drizzle":
                    case "Thunderstorm":
                        RenderSettings.skybox = skyCloud;
                        break;
                    default:
                        RenderSettings.skybox = skySun;
                        break;
                }
            }
        }
        else
        {
            RenderSettings.skybox = skySun;
        }

        Debug.Log("Weather : " + wd.weather[0].description);
    }

    #region Weather Class

    [System.Serializable]
    public class Coord
    {
        public float lon;
        public float lat;
    }

    [System.Serializable]
    public class Main
    {
        public float temp;
        public float pressure;
        public float humidity;
        public float temp_min;
        public float temp_max;
        public float sea_level;
        public float grnd_level;
    }

    [System.Serializable]
    public class Wind
    {
        public float speed;
        public float deg;
    }

    [System.Serializable]
    public class Clouds
    {
        public float all;
    }

    [System.Serializable]
    public class Sys
    {
        public float message;
        public string country;
        public int sunrise;
        public int sunset;
    }

    [System.Serializable]
    public class Weather
    {
        public int id;
        public string main;
        public string description;
        public string icon;
    }

    [System.Serializable]
    public class WeatherData
    {
        public Coord cord;

        public Weather[] weather;

        public string @base;

        public Main main;

        public Wind wind;

        public Clouds clouds;

        public uint dt;

        public Sys sys;

        public int id;

        public string name;

        public int cod;

        public void Load(string savedData)
        {
            JsonUtility.FromJsonOverwrite(savedData, this);
        }
    }

    #endregion
}
